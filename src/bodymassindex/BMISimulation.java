/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodymassindex;

/**
 *
 * @author Eshkarn
 */
public class BMISimulation 
{
    public static void main(String[] args)
    {
        String personBMI = "NAME: %s\nWEIGHT:%.2f\nWEIGHT IN LBS:%.2f\nBODY MASS INDEX:%f\n";
        
        Person person = new Person("Jay", 82, Weight.Lb, 172,Height.In);
        
        
        System.out.printf(personBMI,person.getName(), person.getWeight(), person.getWeightLbs(),person.getBMI());
        System.out.println();
        
        person = new Person("Kane", 71, Weight.Lb, 152, Height.In);
        
        System.out.printf(personBMI,person.getName(), person.getWeight(), person.getWeightLbs(),person.getBMI());
        System.out.println();
        
    }
}
