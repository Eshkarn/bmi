/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bodymassindex;

/**
 *
 * @author Eshkarn
 */
public class Person 
{
    private String name;
    private double weight;
    private double height;
    private Weight unitWeight;
    private Height unitHeight;
    
    protected Person(String name, double weight, Weight unitWeight, double height, Height unitHeight)
    {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.unitWeight = unitWeight;
        this.unitHeight = unitHeight;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public double getWeight()
    {
        return this.weight;
    }
    
    public void setWeight(double weight)
    {
        this.weight = weight;
    }
    
    public double getWeightLbs()
    {
        return this.weight / 2.2;
    }
    
    public double getBMI()
    {
//          double calc = 0;
//          if(unitWeight == Weight.Kg && unitHeight == Height.M)
//          {
//              calc = weight / (Math.pow(height, 2));
//                //return weight / (Math.pow(height, 2));      
//          }
        
        double weightInKilograms = 0;
        double heightInMeters = 0;
        switch(unitWeight)
        {
            case Kg: weightInKilograms = weight;
            break;
            case Lb: weightInKilograms = weight * 0.4535;
            break;
        }
        
        switch(unitHeight)
        {
            case M: heightInMeters = height;
            break;
            case In: heightInMeters = height * 0.254;
            break;
        }
        //return (unitWeight == Weight.K && unitHeight == Height.M) ? weight / (Math.pow(height, 2)) : 0.0;
        return weightInKilograms / (Math.pow(heightInMeters, 2));
        //return calc;
    }
}
